package com.zuitt.example;

public class HelloWorld {
    // In java every standalone program must have a 'main method' as the starting as the starting point for the execution of the program.

    /*
    *  - public: It is an access specifier that indicates the method is accessible from anywhere in the program.
    *  - static: It means the method belongs to the class itself rather than instance of the class. The method needs to be static because it is called by the Java runtime system before any objects are created
    *  - void: It is the return type of the method indicating that the main method does not return anything
    *  - main:It is the name of the method. Tha java runtime system looks for this exact method to start the program
    *  - (String[] args): a parameter severs as a way for java program to receive information or instructions from the outside when it starts running.
    *
    * */

    public static void main(String[] args){
        System.out.println("Hello World");
    }
}
