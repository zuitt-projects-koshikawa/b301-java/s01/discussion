package com.zuitt.example;

public class Variables {
    public static void main(String[] args){
        // variable declaration
        int age;
        char middle_name;

        // variable declaration vs initialization
        int x; // declaration
        int y = 0; // initialization


        System.out.println("The value of y is " + y);
    }
}
